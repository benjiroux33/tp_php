<?php

    include_once('config.php');

    session_start();
    if (isset($_SESSION["IS_CONNECTED"])) {
        header("Location: home.php");
        exit;
    }

?>